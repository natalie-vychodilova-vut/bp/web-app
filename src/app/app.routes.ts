import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import {CreateComponent} from "./create/create.component";
import {RulesComponent} from "./rules/rules.component";
import {EditorComponent} from "./editor/editor.component";

const routes: Routes = [
    {
        path: '',
        redirectTo: '/start',
        pathMatch: 'full'
    },
    {
        path: 'create',
        component: CreateComponent,
    },
    {
        path: 'rules',
        component: RulesComponent,
    },
    {
        path: 'editor/:id',
        component: EditorComponent,
    },
    {
        path: 'editor',
        component: EditorComponent,
    },

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }