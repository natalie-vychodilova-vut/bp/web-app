import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'rule-engine-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'rule-engine';

  /**
  constructor(private router: Router) {
  }

  routerEvent(route: string) {
    this.router.navigate(["./" + route]);
  }
   **/


  constructor(private router: Router, private activatedRoute: ActivatedRoute) {}


}
