import {Component, OnInit} from '@angular/core';
import { Rule } from "../interface/rule";
import { RuleService } from "../service/rule.service";
import { Trigger } from "../interface/trigger";
import { Operand } from "../interface/operand";
import {ToastrService} from "ngx-toastr";
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'rule-engine-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.css'],
})
export class RulesComponent implements OnInit {
  rules: Rule[];

  rule: Rule = { name: '', is_active: false, last_triggerd: ''}

  ruleSend = {id: '', is_active: false }

  data: any;


  constructor( private ruleService: RuleService,
               private toastr: ToastrService,
               private route: ActivatedRoute ) {
  }

  ngOnInit() {
    if (this.route.queryParams !== undefined) {
      this.route.queryParams.subscribe(params => {
        if(params['message'] !== undefined ){
          this.toastr.info(params['message'], 'info');
          this.route.queryParams === null;
        }
      });
    }

    this.loadRules();
  }

  editName(): void {
    for(const i in this.rules){
      this.rules[i].name = this.rules[i].name.replace(/<(.+?)>/g, "<b>$1</b>");
    }
  }

  loadRules(): void {
    this.ruleService.getRules().subscribe(response => {
      this.rules = response;
      this.editName();
    });
  }

  updateStateRule(rule: any): void {
    this.ruleService.updateRule(rule).subscribe(response => {
      this.toastr.info('State of the rule was updated.', 'info');
    });
  }

  postData(rule: any): void {
    this.ruleSend.id = rule.id;
    this.ruleSend.is_active = !rule.is_active
    this.updateStateRule(this.ruleSend)
    rule.is_active = !rule.is_active
  }

  createObject(incoming_block: any) {
    if ('operand_type' in incoming_block) {
      const operand: Operand = {
        operand_type: incoming_block['operand_type'] as string,
        left: this.createObject(incoming_block['left'] as any),
        right: this.createObject(incoming_block['right'] as any)
      };
      return operand;

    } else {
      const trigger: Trigger = {
        id: incoming_block['_id'] as string,
        name: '',
        description: '',
        function: '',
        params: {},
      };
      return trigger;
    }
  }

  createHTMLElements(incoming_block: any) {
    if ('operand_type' in incoming_block) {
      const blockEl = document.createElement('div');
      blockEl.className = 'block';
      blockEl.style.border = '1px solid black';
      blockEl.style.margin = '10px'

      const operandEl = document.createElement('h6');
      operandEl.textContent = incoming_block.operand_type;

      const leftEl = this.createHTMLElements(incoming_block.left);
      const rightEl = this.createHTMLElements(incoming_block.right);

      blockEl.appendChild(leftEl);
      blockEl.appendChild(operandEl);
      blockEl.appendChild(rightEl);

      return blockEl;

    } else {
      const triggerEl = document.createElement('div');
      const triggerID = document.createElement('p');
      const triggerPar = document.createElement('div');


      for (const [key, value] of Object.entries(incoming_block['params'])) {
        const param = document.createElement('p');
        param.textContent = `${key}: ${value}`;
        triggerPar.appendChild(param);
      }

      triggerEl.className = 'trigger';
      triggerEl.style.background = 'blue';

      triggerID.textContent = incoming_block['_id'] as string;

      triggerEl.appendChild(triggerID);
      triggerEl.appendChild(triggerPar);

      return triggerEl;
    }
  }


}
