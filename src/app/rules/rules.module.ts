import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RulesRoutingModule } from "./rules-routing.module";
import { RulesComponent} from "./rules.component";
import {FlexLayoutModule} from "@angular/flex-layout";


@NgModule({
    imports: [
        CommonModule,
        RulesRoutingModule,
        FlexLayoutModule,
    ],
    declarations: [RulesComponent]
})
export class RulesModule { }