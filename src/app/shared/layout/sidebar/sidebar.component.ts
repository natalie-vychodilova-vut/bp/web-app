import { Component } from '@angular/core';
import { faGrip, faCode, faFolderPlus, faCircleUser, faGear } from "@fortawesome/free-solid-svg-icons";

@Component({
    selector: 'rule-engine-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent {
    icons = [faGrip, faCode, faFolderPlus, faCircleUser, faGear];
}