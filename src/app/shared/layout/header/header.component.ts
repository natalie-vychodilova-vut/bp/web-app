import { Component } from '@angular/core';
import { faQuestion } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'rule-engine-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent {
    faQuestion = faQuestion;
    user = "Natalie Wychodilova"
    color = "#"

    getInitials(nameString:any){
        const fullName = nameString.split(' ');
        const initials = fullName.shift().charAt(0) + fullName.pop().charAt(0);
        return initials.toUpperCase();
    }
}