import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditorRoutingModule } from "./editor-routing.module";
import { EditorComponent } from "./editor.component";

import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {FlexLayoutModule } from "@angular/flex-layout";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DndModule} from "ngx-drag-drop";


@NgModule({
    imports: [
        CommonModule,
        EditorRoutingModule,
        FontAwesomeModule,
        FlexLayoutModule,
        FormsModule,
        ReactiveFormsModule,
        DndModule,
    ],
    declarations: [EditorComponent]
})
export class EditorModule { }