import {Component, OnInit, HostBinding } from '@angular/core';

import { RuleService } from "../service/rule.service";
import { Router } from '@angular/router';
import { Action } from "../interface/action";

import {Trigger} from "../interface/trigger";
import { TriggerBlock } from "../interface/trigger-block";


import {SewioApiService} from "../service/sewio-api.service";
import {TemplateDataService} from "../service/template-data.service";

import { ToastrService } from 'ngx-toastr';
import { trigger, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'rule-engine-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css'],
  animations: [
    trigger('flyInOut', [
      transition(':enter', [
        style({ opacity: 0, transform: 'translateX(-100%)' }),
        animate('0.3s ease-out', style({ opacity: 1, transform: 'translateX(0)' })),
      ]),
      transition(':leave', [
        animate('0.3s ease-out', style({ opacity: 0, transform: 'translateX(100%)' })),
      ]),
    ]),
  ],
})
export class EditorComponent implements OnInit{
  @HostBinding('@flyInOut')

  public animatePage = true;

  actions: Action[] = [];
  triggers: Trigger[] = [];
  data: any;


  selectedTrigger: any = null;
  selectedActions = null;


  paramsDataTrigger: any = {};
  selectedValues: any = {}


  zones: { id: any, name: any }[] = [];

  keyFunctionMap: any = {
    'zone_id': this.loadZonesData,
    'tag_id': this.loadTagsData,
    'duration': this.loadNumbers
  }

  allowedParams = ['zone_id', 'duration', 'tag_id']

  actions_block: any = []
  trigger_block: TriggerBlock;

  ruleName: any = {start: 'When ', triggerPart: 'trigger', connector: ' then', actionPart: [], actionString: ' <b>action.</b>'};

  constructor(
      private ruleService: RuleService,
      private sewioService: SewioApiService,
      private templateDataService: TemplateDataService,
      private router: Router,
      private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.trigger_block = {id: '', name: '', data: {}}
    this.data = this.templateDataService.getTemplateData();
    if( this.data !== null && this.data !== undefined){
        this.putTemplateData()
    }
    this.loadActions();
    this.loadTriggers();
  }

  putTemplateData(): void {
    for(const i in this.data.action_templates){
      const newAction = {
        id: this.data.action_templates[i].id,
        name: this.data.action_templates[i].name,
        data: this.data.action_templates[i].data
      }
      this.actions_block.push(newAction)
    }
    this.trigger_block = {
      id: this.data.trigger_template.id,
      name: this.data.trigger_template.name,
      data: this.data.trigger_template.data
    }
    this.selectedValues = {};
    if (this.trigger_block.data.all) {
      this.trigger_block.data.all.forEach((obj: any) => {
        if(this.allowedParams.includes(obj.name)) {
          this.loadParamsData(obj.name)
        }
      });
    }
    this.selectedTrigger = this.trigger_block;
    this.selectedActions = this.actions_block;
  }

  onParamInput(value: any, id: string, key_in: any): void {
    for(const i in this.actions_block){
      const action_id = this.actions_block[i].id;
      if(action_id === id){
        this.actions_block[i].data.params[key_in] = value.value;
      }
    }
  }

  actionName(action: any): void {
    if (!this.ruleName.actionPart.includes(action.name)) {
      this.ruleName.actionPart.push(action.name);
    }
  }

  createActionString(){
    this.ruleName.actionString = ' ';
    for(const i in this.ruleName.actionPart){
      this.ruleName.actionString += '<b>' + this.ruleName.actionPart[i] + '</b>' + ' and '
    }
    this.ruleName.actionString = this.ruleName.actionString.slice(0, -5) + '.';
  }

  onActionSelect(action: any): void {
    this.actionName(action)
    this.createActionString()
    if (!this.actions_block.some((a: any) => a.id === action.id)) {
      const newAction = {
        id: action.id,
        name: action.name,
        data: action.data
      }
      this.actions_block.push(newAction)
    }
  }

  onDeleteAction(a: any): void {
    const index = this.actions_block.findIndex((a: any) => a.id === a.id);
    const indexName = this.ruleName.actionPart.indexOf(a.name)
    if (indexName !== -1) {
      this.ruleName.actionPart.splice(index, 1);
    }
    this.createActionString()
    if (index !== -1) {
      this.actions_block.splice(index, 1);
    }
  }

  onDataSelect(value: any, key: any): void {
    if (this.trigger_block.data.all) {
      this.trigger_block.data.all.forEach((obj: any) => {
        if(obj.name === key) {
          obj.name = key;
          obj.value = value;
        }
      });
    }
  }

  onTriggerSelect(trigger: any): void {
    this.ruleName.triggerPart = trigger.name;
    this.trigger_block = {
      id: trigger.id,
      name: trigger.name,
      data: trigger.data
    }
    this.selectedValues = {};
    if (this.trigger_block.data.all) {
      this.trigger_block.data.all.forEach((obj: any) => {
        if(this.allowedParams.includes(obj.name)) {
          this.loadParamsData(obj.name)
        }
      });
    }
  }

  loadParamsData(key: string): void {
    const loadFunction = this.keyFunctionMap[key];
    if (loadFunction) {
      loadFunction.call(this);
    }
    this.selectedValues[key] = null;
  }

  loadTagsData(): void {
    this.sewioService.getTags().subscribe(
        (response: any) => {
          for (const i in response) {
            for (const j in response[i]){
              const tagAlias = response[i][j].alias;
              if (!this.paramsDataTrigger['tag_id']) {
                this.paramsDataTrigger['tag_id'] = [tagAlias];
              } else {
                this.paramsDataTrigger['tag_id'].push(tagAlias);
              }
            }
          }
        },
        (error: any) => console.log(error)
    );
  }

  loadZonesData(): void {
    this.sewioService.getZones(137, 'coils').subscribe(
        (response: any) => {
          this.zones.length = 0;
          for (const i in response) {
            const zone = {
              id: response[i].id,
              name: response[i].name
            }
            this.zones.push(zone)
          }
          if (!this.paramsDataTrigger['zone_id']) {
            this.paramsDataTrigger['zone_id'] = [];
          }
          this.paramsDataTrigger['zone_id'].length = 0;
          for(const i in this.zones) {
            this.paramsDataTrigger['zone_id'].push(this.zones[i].name);
          }
        },
        (error: any) => console.log(error)
    );
  }
  loadNumbers(): void {
    const numbers: number[] = [...Array(10).keys()].map(x => x + 1);
      if (!this.paramsDataTrigger['duration']) {
        this.paramsDataTrigger['duration'] = [];
      }
      this.paramsDataTrigger['duration'].length = 0;
      for (const i in numbers) {
        this.paramsDataTrigger['duration'].push(i);
      }
  }



  loadTriggers(): void {
    this.ruleService.getTriggersTemplates().subscribe(response => {
      this.triggers = response;
      for (const a in this.triggers){
        this.triggers[a].name = this.triggers[a].name?.replace(/\{.*?\}/g, '');
      }
    });
  }
  loadActions(): void {
    this.ruleService.getActionsTemplates().subscribe(response => {
      this.actions = response;
      for (const a in this.actions){
        this.actions[a].name = this.actions[a].name?.replace(/{|}/g, '');
      }
    });
  }

  sendData(): void {
    if(this.trigger_block !== undefined){
      if (this.trigger_block.data.all) {
        this.trigger_block.data.all.forEach((obj: any) => {
          if(obj.name === 'zone_id') {
            for (const zone of this.zones) {
              if (zone.name === obj.value) {
                obj.value = zone.id as string;
              }
            }
          }
        });
      }
    }

    const actions: any = [];
    this.actions_block.forEach((action: any) => {
      const a = {
        id: action.id,
        data: action.data
      }
      actions.push(a)
    });
    const rule: any = {
      actions_block : actions,
      trigger_block: {
        id: this.trigger_block.id,
        data: this.trigger_block.data
      }
    }
    this.ruleService.createRule(rule).subscribe(
        response => {
          this.router.navigate(['/rules']).then(r => console.log(r));
        },
        error => {
          if(error === 'Data is empty!') {
            this.toastr.warning(error, 'warning');
          }
          if(error === 'This rule already exists!') {
            this.router.navigate(['/rules'] ).then(r => console.log(r));
          }
          this.toastr.error(error, 'error');
        });
  }
}
