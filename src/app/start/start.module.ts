import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StartRoutingModule } from "./start-routing.module";
import { StartComponent } from "./start.component";
import {CdkDrag, CdkDropList} from "@angular/cdk/drag-drop";
import {FlexLayoutModule} from "@angular/flex-layout";


@NgModule({
    imports: [
        CommonModule,
        StartRoutingModule,
        CdkDrag,
        CdkDropList,
        FlexLayoutModule,
    ],
    declarations: [StartComponent]
})
export class StartModule { }