import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { DndModule } from "ngx-drag-drop";

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routes';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { StartModule } from './start/start.module';
import { LayoutModule } from './shared/layout/layout.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CreateModule } from './create/create.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RulesModule } from './rules/rules.module';
import {EditorModule} from "./editor/editor.module";

import { RuleService } from "./service/rule.service";
import { SewioApiService } from "./service/sewio-api.service";

import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot([]),
    AppRoutingModule,
    FormsModule,
    LayoutModule,
    ReactiveFormsModule,
    HttpClientModule,
    TooltipModule.forRoot(),
    StartModule,
    FontAwesomeModule,
    CreateModule,
    RulesModule,
    EditorModule,
    FlexLayoutModule,
    DndModule,
    ToastrModule.forRoot({
      preventDuplicates: true,
      positionClass: 'toast-top-center',
      timeOut: 5000,
      closeButton: true,
    }),
    BrowserAnimationsModule,
  ],
  providers: [RuleService, SewioApiService],
  bootstrap: [AppComponent],
})
export class AppModule {}
