import {Trigger} from "./trigger";

export interface Operand {
    operand_type: string;
    left: Operand | Trigger;
    right: Operand | Trigger;

}
