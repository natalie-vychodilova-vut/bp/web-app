export interface Trigger {
    id?: string
    name?: string
    description?: string
    created?: string
    function?: string
    params:  { [key: string]: string };
}
