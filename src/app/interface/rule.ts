import { Action } from "./action";
import { Block } from "./block";

export interface Rule {
    id?: number
    name: string
    description?: string
    created?: string
    is_active?: boolean
    last_triggerd?: any
    trigger?: any
    actions?: Action[]
}