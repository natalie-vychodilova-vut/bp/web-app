export interface TriggerBlock {
    id: string;
    name: string;
    data: {
        all?: {
            name: string;
            operator: string;
            value: any;
        }[];
        any?:{
            name: string;
            operator: string;
            value: any;
        }[];
    };
}