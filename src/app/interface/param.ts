export interface Param {
    [key: string]: string;
}
