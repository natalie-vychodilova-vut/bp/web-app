import {Component, Input, OnInit} from '@angular/core';
import { RuleService } from "../service/rule.service";
import { HttpEventType, HttpResponse } from "@angular/common/http";

import { EditorComponent } from "../editor/editor.component";
import {Router} from "@angular/router";
import {TemplateDataService} from "../service/template-data.service";

@Component({
  moduleId: module.id,
  selector: 'rule-engine-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit{
  templates: any;
  constructor(private ruleService: RuleService,
              private templateDataService: TemplateDataService) {}

  ngOnInit(): void {
    this.loadTemplates()
  }

  loadTemplates(): void {
    this.ruleService.getRuleTemplates().subscribe(response => {
      this.templates = response;
    });
  }

  sendTemplateData(data: any){
    if(data === null) {
      this.templateDataService.removeTemplateData()
    }
    this.templateDataService.setTemplateData(data)
  }
}
