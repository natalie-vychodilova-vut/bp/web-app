import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateRoutingModule } from "./create-routing.module";
import { CreateComponent } from "./create.component";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";


@NgModule({
    imports: [
        CommonModule,
        CreateRoutingModule,
        FontAwesomeModule,
    ],
    declarations: [CreateComponent]
})
export class CreateModule { }