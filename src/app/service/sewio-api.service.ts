import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {Rule} from "../interface/rule";


@Injectable({
  providedIn: 'root'
})
export class SewioApiService {

  private apiUrl = environment.sewioApiUrl;
  private apiKey = environment.sewioApiKey;
  constructor(private http: HttpClient) {}

  public getZones(buildingId: number, plan: string): Observable<any> {
    const url = `${this.apiUrl}/buildings/${buildingId}/plans/${plan}/zones`;
    const headers = new HttpHeaders().set('X-ApiKey', this.apiKey);
    return this.http.get(url, { headers });
  }
  public getTags(): Observable<any> {
    const url = `${this.apiUrl}/tags`;
    const headers = new HttpHeaders().set('X-ApiKey', this.apiKey);
    return this.http.get(url, { headers });
  }





}
