import { TestBed } from '@angular/core/testing';

import { SewioApiService } from './sewio-api.service';

describe('SewioapiService', () => {
  let service: SewioApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SewioApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
