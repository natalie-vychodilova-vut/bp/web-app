import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable, throwError } from "rxjs";

import { Rule } from "../interface/rule";
import {environment} from "../../environments/environment";
import { catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class RuleService {

  constructor(private http: HttpClient) {}

  private apiUrl = environment.apiUrl;

  public getTriggersTemplates(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/templates/triggers`);
  }

  public getActionsTemplates(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/templates/actions`);
  }

  public getRules(): Observable<Rule[]> {
    return this.http.get<Rule[]>(`${this.apiUrl}/rules`);
  }

  public getRule(rule: Rule): Observable<Rule> {
    return this.http.get<Rule>(`${this.apiUrl}/rules/${rule.id}`);
  }

  public createRule(rule: any): Observable<Rule> {
    return this.http.post<any>(`${this.apiUrl}/rules_all`, rule).pipe(
        catchError((error: HttpErrorResponse) => {
          if (error.status === 400) {
            return throwError(error.error[0]);
          }
          return throwError('Something went wrong. Please try again later.');
        })
    );
  }

  public updateRule(rule: Rule): Observable<Rule> {
    return this.http.put<Rule>(`${this.apiUrl}/rules/${rule.id}`, rule);
  }


  public getRuleTemplates(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/templates/rules`);
  }

  public getTemplate(objectId: string): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/rules/templates/${objectId}`);
  }

}
