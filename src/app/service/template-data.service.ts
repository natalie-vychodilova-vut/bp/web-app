import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class TemplateDataService {
    private data: any;
    setTemplateData(data: any) {
        this.data = data;
    }
    getTemplateData() {
        return this.data;
    }

    removeTemplateData() {
        this.data = null;
    }
}
